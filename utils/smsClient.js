const axios = require("axios");

const tlClient = axios.create({
  baseURL: "https://api.textlocal.in/",
  params: {
    apiKey: "NjQ2MzU4NzM1MzY5NjQ3MTQ3NmY3MDcxNDQzNDMyNGQ=",
    sender: "MPBIO",
  },
});

const smsClient = {
  sendVerificationMessage: (user) => {
    if (user && user.phone) {
      const params = new URLSearchParams();
      params.append("numbers", [parseInt("91" + user.phone)]);
      params.append(
        "message",
        `Dear customer, welcome! OTP for registering Manakattampally bio science is ${user.code}. OTP is valid for 3 minutes.`
      );
      tlClient.post("/send", params);
    }
  },
  sendOrderConfirmationMessage: (user, order) => {
    if (user && user.mobile && order) {
      let totalAmount = order.orderTotal + order.shippingTotal;
      let amountTrimmed = totalAmount.toString();
      if (amountTrimmed.length > 4) {
        amountTrimmed = amountTrimmed.substring(0, 4);
      }
      let delivstart = formatAMPM(order.timeslot.startTime);
      let delivendt = formatAMPM(order.timeslot.endTime);
      const params = new URLSearchParams();
      params.append("numbers", [parseInt("91" + user.mobile)]);
      params.append(
        "message",
        `Dear customer, we have recieved order for Rs.${amountTrimmed}. Expected delivery ${delivstart} - ${delivendt} -Manakattampally bio`
      );

      return new Promise((resolve) => {
        tlClient
          .post("/send", params)
          .then(function (response) {
            console.log(response.data);
            resolve(response.data.status);
          })
          .catch(function (error) {
            resolve("Error in sending sms");
          });
      });
    }
  },
  sendTestMessage: () => {
    let totalAmount = 12345;
    let amountTrimmed = totalAmount.toString();
    amountTrimmed = amountTrimmed.substring(0, 4);
    let delivstart = formatAMPM("10:15");
    let delivendt = formatAMPM("11:15");

    const params = new URLSearchParams();
    params.append("numbers", [parseInt("919995336538")]);
    params.append(
      "message",
      `Dear customer, we have recieved order for Rs.${amountTrimmed}. Expected delivery ${delivstart} - ${delivendt} -Manakattampally bio`
    );

    return new Promise((resolve) => {
      tlClient
        .post("/send", params)
        .then(function (response) {
          console.log(response.data);
          resolve(response.data.status);
        })
        .catch(function (error) {
          resolve("Error in sending sms");
        });
    });
  },
  test: () => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve("resolved");
      }, 2000);
    });
  },
};

function formatAMPM(time) {
  var hours = parseInt(time.split(":")[0]);
  var minutes = parseInt(time.split(":")[1]);

  var ampm = hours >= 12 ? "pm" : "am";
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? "0" + minutes : minutes;
  // var strTime = hours + ":" + minutes + " " + ampm;
  var strTime = hours + ampm;
  return strTime;
}

module.exports = smsClient;
