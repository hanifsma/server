var AWS = require("aws-sdk");
AWS.config.loadFromPath("./config/config.json");

AWS.config.update({ region: "us-east-2" });
s3 = new AWS.S3({ apiVersion: "2006-03-01" });
var uploadParams = {
  Bucket: "fishbio",
  Key: "",
  Body: "",
  ACL: "public-read",
  ContentEncoding: "base64",
  ContentType: "image/jpeg",
};

const uploadFiles = (fileStreams) => {
  let result = [];
  return new Promise((resolve, reject) => {
    fileStreams.forEach((element) => {
      var base64data = Buffer.from(
        element.src.replace(/^data:image\/\w+;base64,/, ""),
        "base64"
      );
      uploadParams.Body = base64data;
      uploadParams.Key = makeid(8) + ".png";
      s3.upload(uploadParams, function (err, data) {
        if (err) {
          console.log("Error", err);
          return reject(err);
        }
        if (data) {
          result.push(data);
        }
        if (result.length == fileStreams.length) {
          return resolve(result);
        }
      });
    });
  });
};

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

exports.uploadFiles = uploadFiles;
