const Joi = require("joi");
const { max } = require("moment");
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const { categorySchema } = require("./category");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 255,
  },
  price: {
    type: Number,
    required: true,
    min: 0,
  },
  category: {
    type: categorySchema,
    required: true,
  },
  unit: {
    type: String,
    required: true,
    minlength: 1,
    maxlength: 10,
  },
  discount: {
    type: Number,
    required: true,
    min: 0,
    max: 100,
  },
  numberInStock: {
    type: Number,
    required: true,
    min: 0,
  },
  isCombo: {
    type: Boolean,
    required: true,
    default: false,
  },
  description: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 5000,
  },
  isAvailable: {
    type: Boolean,
    required: true,
    default: true,
  },
  stepper: {
    type: Number,
    required: true,
    min: 0.1,
    max: 500,
    default: 1,
  },
  images: {
    type: [String],
  },
  quantity: {
    type: Number,
    default: 0,
  },
});

productSchema.plugin(mongoosePaginate);

productSchema.method("transform", function () {
  var obj = this.toObject();

  obj.id = obj._id;
  delete obj._id;

  return obj;
});

productSchema.method("transformAll", function () {
  var obj = this.toObject();

  obj.id = obj._id;
  delete obj._id;

  obj.category.id = obj.category._id;
  delete obj.category._id;

  delete obj.__v;

  // TODO update images DB
  let images = [];
  for (let j = 0; j < obj.images.length; j++) {
    images.push({ id: j + 1, url: obj.images[j] });
  }
  obj.images = images;

  return obj;
});

productSchema.method("addKey", function (key) {
  var obj = this.toObject();

  obj.key = key.toString();

  return obj;
});

const Product = mongoose.model("Products", productSchema);

function validateProduct(product) {
  const schema = Joi.object({
    name: Joi.string().min(3).max(50).required(),
    price: Joi.number().min(0).required(),
    category: Joi.object().required(),
    unit: Joi.string().min(1).max(10).required(),
    discount: Joi.number().min(0).max(100).required(),
    numberInStock: Joi.number().min(0).required(),
    isCombo: Joi.boolean().required().default(false),
    description: Joi.string().min(3).max(5000).required(),
    isAvailable: Joi.boolean().required().default(true),
    stepper: Joi.number().min(0.1).max(500).required().default(1),
    images: Joi.array().default([]),
    quantity: Joi.number(),
    id: Joi.objectId(),
  });

  return schema.validate(product);
}

exports.productSchema = productSchema;
exports.Product = Product;
exports.validate = validateProduct;
