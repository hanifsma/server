const Joi = require("joi");
const mongoose = require("mongoose");

const agentSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 50,
  },
});

const Agent = mongoose.model("Agent", agentSchema);

function validateAgent(agent) {
  const schema = Joi.object({
    name: Joi.string().min(3).max(50).required(),
  });

  return schema.validate(agent);
}

exports.agentSchema = agentSchema;
exports.Agent = Agent;
exports.validate = validateAgent;
