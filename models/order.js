const Joi = require("joi");
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const { productSchema } = require("./product");
const { userSchema, addressSchema } = require("./user");
const { timeslotSchema } = require("./timeslot");

const orderSchema = new mongoose.Schema({
  orderId: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 20,
  },
  user: {
    type: String,
    required: true,
  },
  address: {
    type: addressSchema,
    required: true,
  },
  products: {
    type: [productSchema],
    required: true,
  },
  shipStatus: {
    type: String,
    minlength: 3,
  },
  paymentStatus: {
    type: String,
    minlength: 3,
  },
  orderTotal: {
    type: Number,
    required: true,
    min: 0,
  },
  date: {
    type: Date,
    required: true,
  },
  paymentOrderId: {
    type: String,
    required: true,
  },
  paymentPayId: {
    type: String,
  },
  paymentSignature: {
    type: String,
  },
  timeslot: {
    type: timeslotSchema,
  },
  shippingTotal: {
    type: Number,
    min: 0,
  },
});

orderSchema.plugin(mongoosePaginate);

orderSchema.method("transform", function () {
  var obj = this.toObject();

  obj.id = obj._id;
  delete obj._id;

  return obj;
});

orderSchema.method("addKey", function (key) {
  var obj = this.toObject();

  obj.key = key.toString();

  return obj;
});

orderSchema.method("transformWithProducts", function (products) {
  var obj = this.toObject();

  obj.products = products;

  obj.id = obj._id;
  delete obj._id;

  return obj;
});

const Order = mongoose.model("Orders", orderSchema);

function validateOrder(order) {
  const schema = Joi.object({
    orderId: Joi.string().min(3).max(20).required(),
    shipStatus: Joi.string()
      .valid("Ordered", "Packed", "Shipped", "Delivered", "Cancelled")
      .required(),
    paymentStatus: Joi.string()
      .valid("Success", "Failed", "Pending", "COD", "Refunded", "OOR")
      .required(),
    products: Joi.array().min(1).required(),
    address: Joi.object().required(),
    date: Joi.date().required(),
    shopFrom: Joi.string(),
    timeslot: Joi.object(),
    shippingTotal: Joi.number(),
  });

  return schema.validate(order);
}

function validateStatus(order) {
  const schema = Joi.object({
    shipStatus: Joi.string()
      .valid("Ordered", "Packed", "Shipped", "Delivered", "Cancelled")
      .required(),
    paymentStatus: Joi.string()
      .valid("Success", "Failed", "Pending", "COD", "Refunded", "OOR")
      .required(),
  });

  return schema.validate(order);
}

function validateVerifyPurchase(order) {
  const schema = Joi.object({
    _id: Joi.objectId().required(),
    paymentPayId: Joi.string().required(),
    paymentSignature: Joi.string().required(),
  });

  return schema.validate(order);
}

function validateUpdate(order) {
  const schema = Joi.object({
    _id: Joi.objectId().required(),
    paymentStatus: Joi.string()
      .valid("Success", "Failed", "Pending", "COD", "Refunded", "OOR")
      .required(),
    timeslot: Joi.object().required(),
    sendsms: Joi.boolean().default(false),
  });

  return schema.validate(order);
}

exports.orderSchema = orderSchema;
exports.Order = Order;
exports.validate = validateOrder;
exports.validateStatus = validateStatus;
exports.validateVerifyPurchase = validateVerifyPurchase;
exports.validateUpdate = validateUpdate;
