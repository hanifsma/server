const Joi = require("joi");
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const schema = new mongoose.Schema({
  image: {
    type: String,
    required: true,
  },
  link: {
    type: String,
    required: true,
  },
  available: {
    type: Boolean,
    required: true,
    default: true,
  },
});

schema.plugin(mongoosePaginate);

schema.method("transform", function () {
  var obj = this.toObject();

  obj.id = obj._id;
  delete obj._id;

  delete obj.__v;

  return obj;
});

schema.method("transformWithImage", function () {
  var obj = this.toObject();

  obj.id = obj._id;
  delete obj._id;

  delete obj.__v;

  // TODO update images DB
  obj.image = { id: 1, url: obj.image };

  return obj;
});

const Promotion = mongoose.model("Promotion", schema);

function validate(promotion) {
  const schema = Joi.object({
    image: Joi.object(),
    link: Joi.string().required(),
    available: Joi.boolean().required().default(true),
  });
  return schema.validate(promotion);
}

function validateEdit(promotion) {
  const schema = Joi.object({
    image: Joi.object().required(),
    link: Joi.string().required(),
    available: Joi.boolean().required().default(true),
    id: Joi.objectId(),
  });
  return schema.validate(promotion);
}

exports.Promotion = Promotion;
exports.validate = validate;
exports.validateEdit = validateEdit;
