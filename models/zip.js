const Joi = require("joi");
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const zipSchema = new mongoose.Schema({
  _id: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 6,
  },
  city: {
    type: String,
    required: true,
    minlength: 1,
    maxlength: 20,
  },
  state: {
    type: String,
    required: true,
    minlength: 1,
    maxlength: 20,
  },
  loc: {
    type: Array,
    required: false,
  },
  available: {
    type: Boolean,
    default: false,
    required: true,
  },
  outOfReach: {
    type: Boolean,
    default: false,
    required: true,
  },
});

zipSchema.plugin(mongoosePaginate);

zipSchema.method("transform", function () {
  var obj = this.toObject();

  obj.id = obj._id;
  delete obj._id;
  delete obj.__v;

  return obj;
});

const Zip = mongoose.model("Zip", zipSchema);

function validateZip(zip) {
  const schema = Joi.object({
    id: Joi.string().min(6).max(6).required(),
    city: Joi.string().min(1).max(20).required(),
    state: Joi.string().min(1).max(20).required(),
    available: Joi.boolean().required(),
    outOfReach: Joi.boolean().required(),
    loc: Joi.array(),
  });

  return schema.validate(zip);
}

exports.zipSchema = zipSchema;
exports.Zip = Zip;
exports.validate = validateZip;
