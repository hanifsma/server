const Joi = require("joi");
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const timeslotSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 50,
  },
  startTime: {
    type: String,
    required: true,
  },
  endTime: {
    type: String,
    required: true,
  },
  available: {
    type: Boolean,
    required: true,
    default: true,
  },
  date: {
    type: Date,
  },
});

timeslotSchema.plugin(mongoosePaginate);

timeslotSchema.method("transform", function () {
  var obj = this.toObject();

  obj.id = obj._id;
  delete obj._id;
  delete obj.__v;

  return obj;
});

const Timeslot = mongoose.model("timeslotSchema", timeslotSchema);

function validate(timeslot) {
  const schema = Joi.object({
    name: Joi.string().min(3).max(50).required(),
    startTime: Joi.string().min(5).max(5).required(),
    endTime: Joi.string().min(5).max(5).required(),
    available: Joi.boolean().required(),
    date: Joi.date(),
  });

  return schema.validate(timeslot);
}

function validateEdit(timeslot) {
  const schema = Joi.object({
    name: Joi.string().min(3).max(50).required(),
    startTime: Joi.string().min(5).max(5).required(),
    endTime: Joi.string().min(5).max(5).required(),
    available: Joi.boolean().required(),
    id: Joi.objectId().required(),
    date: Joi.date(),
  });

  return schema.validate(timeslot);
}

exports.Timeslot = Timeslot;
exports.validate = validate;
exports.validateEdit = validateEdit;
exports.timeslotSchema = timeslotSchema;
