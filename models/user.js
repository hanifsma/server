const config = require("config");
const jwt = require("jsonwebtoken");
const Joi = require("joi");
const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const { productSchema } = require("./product");
const joiPhoneNumber = Joi.extend(require("joi-phone-number"));

const addressSchema = new mongoose.Schema({
  name: String,
  mobile: String,
  pin: String,
  houseNumber: String,
  street: String,
  landmark: String,
  city: String,
  state: String,
  deleted: Boolean,
});

addressSchema.method("addKey", function (key) {
  var obj = this.toObject();
  obj.key = key.toString();
  return obj;
});

const otpSchema = new mongoose.Schema({
  otp: {
    type: String,
    required: true,
  },
  verified: {
    type: Boolean,
    required: true,
  },
  date: {
    type: Date,
    required: true,
  },
});

const userSchema = new mongoose.Schema({
  mobile: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: false,
    minlength: 5,
    maxlength: 1024,
  },
  name: {
    type: String,
    required: false,
    minlength: 3,
    maxlength: 50,
  },
  email: {
    type: String,
    required: false,
  },
  address: {
    type: [addressSchema],
    required: false,
  },
  cart: {
    type: [productSchema],
  },
  otp: {
    type: otpSchema,
  },
  joiningDate: {
    type: Date,
  },
  isAdmin: Boolean,
});

userSchema.plugin(mongoosePaginate);

userSchema.methods.generateAuthToken = function () {
  const token = jwt.sign(
    {
      _id: this._id,
      isAdmin: this.isAdmin,
      name: this.name,
      mobile: this.mobile,
      email: this.email,
    },
    config.get("jwtPrivateKey")
  );
  return token;
};

userSchema.method("transform", function () {
  var obj = this.toObject();

  obj.id = obj._id;
  delete obj._id;

  return obj;
});

const User = mongoose.model("User1", userSchema);

function validateUser(user) {
  const schema = Joi.object({
    mobile: joiPhoneNumber
      .string()
      .phoneNumber({ defaultCountry: "IN", strict: true })
      .required(),
    email: Joi.string().email({
      minDomainSegments: 2,
      tlds: { allow: ["com", "net"] },
    }),
    name: Joi.string().alphanum().min(3).max(50),
  });

  return schema.validate(user);
}

function validateAddress(address) {
  const schema = Joi.object({
    name: Joi.string().min(3).max(50).required(),
    mobile: joiPhoneNumber
      .string()
      .phoneNumber({ defaultCountry: "IN", strict: true })
      .required(),
    pin: Joi.number().min(100000).max(999999).required(),
    houseNumber: Joi.string().min(3).max(255).required(),
    street: Joi.string().min(3).max(255).required(),
    landmark: Joi.string().min(3).max(255),
    city: Joi.string().min(3).max(255),
    state: Joi.string().min(3).max(255),
  });

  return schema.validate(address);
}

function validateCart(cart) {
  const schema = Joi.object({
    productId: Joi.objectId().required(),
    quantity: Joi.number().min(0).required(),
  });

  return schema.validate(cart);
}

function validateProductId(product) {
  const schema = Joi.object({
    productId: Joi.objectId().required(),
  });

  return schema.validate(product);
}

exports.addressSchema = addressSchema;
exports.userSchema = userSchema;
exports.User = User;
exports.validate = validateUser;
exports.validateAddress = validateAddress;
exports.validateCart = validateCart;
exports.validateProductId = validateProductId;

// password: Joi.string()
//   .pattern(
//     new RegExp("^(?=.*[A-z])(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,}$")
//   )
//   .required(),
