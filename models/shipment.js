const Joi = require("joi");
const mongoose = require("mongoose");
const { agentSchema } = require("./agent");
const { orderSchema } = require("./order");

const shipmentSchema = new mongoose.Schema({
  agent: {
    type: agentSchema,
    required: true,
  },
  order: {
    type: orderSchema,
    required: true,
  },
  pickupTime: {
    type: Date,
  },
  deliverTime: {
    type: Date,
  },
});

const Shipment = mongoose.model("Shipments", shipmentSchema);

function validateShipment(shipment) {
  const schema = Joi.object({
    agentId: Joi.objectId().required(),
    orderId: Joi.objectId().required(),
    pickupTime: Joi.date(),
    deliverTime: Joi.date(),
  });

  return schema.validate(shipment);
}

function validateShipmentStatus(shipment) {
  const schema = Joi.object({
    pickupTime: Joi.date().required(),
    deliverTime: Joi.date().required(),
  });

  return schema.validate(shipment);
}

exports.shipmentSchema = shipmentSchema;
exports.Shipment = Shipment;
exports.validate = validateShipment;
exports.validateStatus = validateShipmentStatus;
