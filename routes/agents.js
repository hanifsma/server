const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const validateObjectId = require("../middleware/validateObjectId");
const { Agent, validate } = require("../models/agent");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  const agents = await Agent.find();
  res.send(agents);
});

router.post("/", [auth, admin, validateObjectId], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let agent = new Agent({ name: req.body.name });
  agent = await agent.save();

  res.send(agent);
});

router.put("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const agent = await Agent.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
    },
    { new: true }
  );

  if (!agent)
    return res.status(404).send("The agent with the given ID was not found.");

  res.send(agent);
});

router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const agent = await Agent.findByIdAndRemove(req.params.id);

  if (!agent)
    return res.status(404).send("The agent with the given ID was not found.");

  res.send(agent);
});

router.get("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const agent = await Agent.findById(req.params.id);

  if (!agent)
    return res.status(404).send("The agent with the given ID was not found.");

  res.send(agent);
});

module.exports = router;
