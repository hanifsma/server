const express = require("express");
const router = express.Router();
const smsClient = require("../utils/smsClient");

router.get("/", async (req, res) => {
  const message = await smsClient.sendTestMessage();

  res.send(message);
});

module.exports = router;
