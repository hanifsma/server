const auth = require("../middleware/auth");
const validateObjectId = require("../middleware/validateObjectId");
const jwt = require("jsonwebtoken");
const config = require("config");
const bcrypt = require("bcrypt");
const _ = require("lodash");
const {
  User,
  validate,
  validateAddress,
  validateCart,
  validateProductId,
} = require("../models/user");
const { Product } = require("../models/product");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();
const smsClient = require("../utils/smsClient");
const admin = require("../middleware/admin");
const moment = require("moment");

router.get("/me", auth, async (req, res) => {
  const user = await User.findById(req.user._id).select("-password");
  res.send(user);
});

router.get("/", [auth, admin], async (req, res) => {
  let query = {};
  if (req.query.q) {
    query.name = new RegExp(req.query.q, "i");
  }
  if (req.query.id) {
    query._id = req.query.id;
  }
  if (req.query.date_gte) {
    const startDate = new Date(req.query.date_gte);
    query.joiningDate = { $gt: startDate };
  }
  let offset = Number(req.query._start);
  let limit = Number(req.query._end) - offset;

  if (!offset) offset = 0;
  if (!limit) limit = 9999;

  await User.paginate(
    query,
    { offset: offset, limit: limit, sort: { _id: -1 } },
    (err, result) => {
      if (result) {
        let users = [];
        for (let i = 0; i < result.docs.length; i++) {
          users.push(result.docs[i].transform());
        }
        res
          .header("X-Total-Count", result.total)
          .header("Access-Control-Expose-Headers", "X-Total-Count")
          .send(users);
      } else {
        res.status(400).send("No results");
      }
    }
  );
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let user = await User.findOne({ mobile: req.body.mobile });
  // if (user) return res.status(400).send("User already registered.");

  let code = generateOTP();

  if (req.body.mobile == "9995336538") {
    code = "1234";
  }

  if (user == null) {
    user = new User(
      _.pick(req.body, ["name", "email", "password", "mobile", "otp"])
    );
    // const salt = await bcrypt.genSalt(10);
    // user.password = await bcrypt.hash(user.password, salt);
    user.joiningDate = Date.now();
    user.otp = {
      otp: code,
      verified: false,
      date: Date.now(),
    };
  } else {
    user.otp = {
      otp: code,
      verified: false,
      date: Date.now(),
    };
  }

  await user.save();

  smsClient.sendVerificationMessage({ phone: user.mobile, code: code });

  const token = user.generateAuthToken();
  res
    .header("x-auth-token", token)
    .send(_.pick(user, ["_id", "name", "email", "mobile"]));
});

router.post("/address", auth, async (req, res) => {
  const { error } = validateAddress(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const user = await User.findById(req.user._id).select("-password");
  if (!user)
    return res.status(404).send("The user with the given ID was not found.");

  const address = user.address;
  address.push(req.body);

  const updatedUser = await User.update(
    { _id: req.user._id },
    {
      address: address,
    }
  );

  res.send(address);
});

router.post("/address/delete", auth, async (req, res) => {
  const user = await User.findById(req.user._id).select("-password");
  if (!user)
    return res.status(404).send("The user with the given ID was not found.");

  const address = user.address;
  for (let i = 0; i < address.length; i++) {
    if (address[i]._id == req.body._id) {
      address[i].deleted = true;
    }
  }

  const updatedUser = await User.update(
    { _id: req.user._id },
    {
      address: address,
    }
  );

  res.send(address);
});

router.get("/me/address", auth, async (req, res) => {
  let addresses = [];
  const user = await User.findById(req.user._id, function (err, result) {
    for (let i = 0; i < result.address.length; i++) {
      if (
        result.address[i].deleted == false ||
        result.address[i].deleted == undefined
      ) {
        addresses.push(result.address[i].addKey(i + 1));
      }
    }
    res.send(addresses.reverse());
  }).select("-password");
});

router.get("/me/cart", auth, async (req, res) => {
  let carts = [];
  const user = await User.findById(req.user._id, function (err, result) {
    for (let i = 0; i < result.cart.length; i++) {
      carts.push(result.cart[i].addKey(i + 1));
    }
    res.send(carts);
  }).select("-password");
});

router.post("/cart", auth, async (req, res) => {
  const { error } = validateCart(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const user = await User.findById(req.user._id).select("-password");
  if (!user)
    return res.status(404).send("The user with the given ID was not found.");

  const product = await Product.findById(req.body.productId);
  if (!product) return res.status(400).send("The product not found.");

  const cart = user.cart;

  let productExist = false;
  for (let i = 0; i < cart.length; i++) {
    if (cart[i]._id.equals(product._id)) {
      cart[i].quantity += req.body.quantity;
      productExist = true;
      break;
    }
  }

  if (!productExist) {
    product.quantity = req.body.quantity;
    cart.push(product);
  }

  const updatedUser = await User.update(
    { _id: req.user._id },
    {
      cart: cart,
    }
  );

  res.send(cart);
});

router.post("/cart/remove", auth, async (req, res) => {
  const { error } = validateProductId(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const user = await User.findById(req.user._id).select("-password");
  if (!user)
    return res.status(404).send("The user with the given ID was not found.");

  // const product = await Product.findById(req.body.productId);
  // if (!product) return res.status(400).send("The product not found.");
  // if (element !== null && element._id.equals(product._id)) {

  const cart = user.cart;
  let index = -1;
  for (let i = 0; i < cart.length; i++) {
    let element = cart[i];
    if (element !== null && element._id.equals(req.body.productId)) {
      index = i;
      break;
    }
  }
  if (index == -1) return res.status(400).send("The product not in cart.");

  cart.splice(index, 1);
  const updatedUser = await User.update(
    { _id: req.user._id },
    {
      cart: cart,
    }
  );

  res.send(cart);
});

router.post("/cart/empty", auth, async (req, res) => {
  const user = await User.findById(req.user._id).select("-password");
  if (!user)
    return res.status(404).send("The user with the given ID was not found.");

  const cart = user.cart;
  const updatedUser = await User.update(
    { _id: req.user._id },
    {
      cart: [],
    }
  );

  res.send("Cart is Empty");
});

router.post("/cart/update", auth, async (req, res) => {
  const { error } = validateCart(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const user = await User.findById(req.user._id).select("-password");
  if (!user)
    return res.status(404).send("The user with the given ID was not found.");

  const product = await Product.findById(req.body.productId);
  if (!product) return res.status(400).send("The product not found.");

  const cart = user.cart;
  let index = -1;
  for (let i = 0; i < cart.length; i++) {
    let element = cart[i];
    if (element !== null && element._id.equals(product._id)) {
      index = i;
      break;
    }
  }
  if (index == -1) return res.status(400).send("The product not in cart.");

  cart[index].quantity = req.body.quantity;
  const updatedUser = await User.update(
    { _id: req.user._id },
    {
      cart: cart,
    }
  );

  res.send(cart);
});

router.post("/verify", async (req, res) => {
  const user = await User.findById(req.body._id);
  if (!user)
    return res.status(400).send("The user with given ID was not found");

  const startDate = new Date(user.otp.date);
  const endDate = moment(startDate).add(3, "minutes").toDate();
  console.log(startDate, endDate);

  if (user.otp.otp != req.body.otp)
    return res.status(400).send("The OTP is not correct");

  if (Date.now() > endDate) {
    return res.status(400).send("Invalid OTP, Please resend");
  }

  await User.update(
    { _id: req.body._id },
    {
      $set: {
        "otp.verified": true,
      },
    }
  );

  user.otp.verified = true;
  const token = user.generateAuthToken();
  res
    .header("x-auth-token", token)
    .send(_.pick(user, ["_id", "name", "email", "mobile"]));
});

router.post("/resendotp", async (req, res) => {
  const user = await User.findById(req.body._id);
  if (!user)
    return res.status(400).send("The user with given ID was not found");

  const code = generateOTP();
  user.otp = {
    otp: code,
    verified: false,
    date: Date.now(),
  };

  await user.save();

  smsClient.sendVerificationMessage({ phone: user.mobile, code: user.otp.otp });

  res.send({
    message: "Otp" + user.otp.otp + " send to mobile " + user.mobile,
    mobile: user.mobile,
  });
});

router.post("/recover", async (req, res) => {
  const users = await User.find({ mobile: req.body.mobile });
  if (!users || users.length == 0)
    return res.status(400).send("The user with given mobile was not found");

  const user = users[0];

  const code = generateOTP();
  user.otp = {
    otp: code,
    verified: false,
    date: Date.now(),
  };

  await user.save();

  smsClient.sendVerificationMessage({ phone: user.mobile, code: user.otp.otp });

  res.send(user._id);
});

router.post("/recoververify", async (req, res) => {
  const user = await User.findById(req.body._id);
  if (!user)
    return res.status(400).send("The user with given ID was not found");

  if (user.otp.otp != req.body.otp)
    return res.status(400).send("The OTP is not correct");

  res.send("verified");
});

router.post("/updatepassword", async (req, res) => {
  const user = await User.findById(req.body._id);
  if (!user)
    return res.status(400).send("The user with given ID was not found");

  if (user.otp.otp != req.body.otp)
    return res.status(400).send("The OTP is not correct");

  const salt = await bcrypt.genSalt(10);
  const newPassword = await bcrypt.hash(req.body.password, salt);

  await User.update(
    { _id: req.body._id },
    {
      $set: {
        password: newPassword,
      },
    }
  );

  const token = user.generateAuthToken();
  res
    .header("x-auth-token", token)
    .send(_.pick(user, ["_id", "name", "email", "mobile"]));
});

router.delete("/:id", async (req, res) => {
  const category = await User.findByIdAndRemove(req.params.id);

  if (!category)
    return res
      .status(404)
      .send("The category with the given ID was not found.");

  res.send(category);
});

router.get("/:id", [validateObjectId, auth, admin], async (req, res) => {
  const user = await User.findById(req.params.id);

  if (!user)
    return res.status(404).send("The user with the given ID was not found.");

  res.send(user.transform());
});

function generateOTP() {
  var digits = "0123456789";
  let OTP = "";
  for (let i = 0; i < 4; i++) {
    OTP += digits[Math.floor(Math.random() * 10)];
  }
  return OTP;
}

module.exports = router;
