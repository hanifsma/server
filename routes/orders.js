const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const validateObjectId = require("../middleware/validateObjectId");
const {
  Order,
  validate,
  validateStatus,
  validateVerifyPurchase,
  validateUpdate,
} = require("../models/order");
const { Product } = require("../models/product");
const { User } = require("../models/user");
const orderId = require("order-id")("fishbio");
const express = require("express");
const router = express.Router();
const Fawn = require("fawn");
const Razorpay = require("razorpay");
const crypto = require("crypto");
const moment = require("moment");
const smsClient = require("../utils/smsClient");

// const KEYID = "rzp_test_WFHVrCFQEijI0f";
// const KEYSECRET = "xYQAathXEOWbygWKv6IOnkOy";

const KEYID = "rzp_live_8mDDa8Do5a7bhi";
const KEYSECRET = "xl6pmHQv8j7NKG1TqS9tZbwq";

router.get("/", [auth], async (req, res) => {
  let query = {};
  if (req.query.shipStatus) {
    query.shipStatus = req.query.shipStatus;
  }
  if (req.query.user) {
    query.user = req.query.user;
  }
  if (req.query.date_gte) {
    const startDate = new Date(req.query.date_gte);
    query.date = { $gt: startDate };
  }
  if (req.query.date) {
    const startDate = new Date(req.query.date);
    const endDate = moment(startDate).endOf("day").toDate();
    query.date = { $gt: startDate, $lt: endDate };
  }
  let offset = Number(req.query._start);
  let limit = Number(req.query._end) - offset;

  if (!offset) offset = 0;
  if (!limit) limit = 9999;

  await Order.paginate(
    query,
    { offset: offset, limit: limit, sort: { _id: -1 } },
    (err, result) => {
      if (result) {
        let orders = [];
        for (let i = 0; i < result.docs.length; i++) {
          orders.push(result.docs[i].transform());
        }
        res
          .header("X-Total-Count", result.total)
          .header("Access-Control-Expose-Headers", "X-Total-Count")
          .send(orders);
      } else {
        return res.status(400).send("No results");
      }
    }
  );
});

router.post("/", [auth], async (req, res) => {
  req.body.orderId = orderId.generate();
  req.body.date = Date.now();
  req.body.shipStatus = "Ordered";

  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const user = await User.findById(req.user._id);
  if (!user) return res.status(400).send("Invalid User");

  let products = [];
  let totalAmount = 0;
  let shippingTotal = req.body.shippingTotal;

  for await (const item of req.body.products) {
    const product = await Product.findById(item.id);
    if (!product) return res.status(400).send("Invalid Product");

    if (product.numberInStock < item.quantity)
      return res.status(400).send("Out of stock");

    product.quantity = item.quantity;
    products.push(product);
    totalAmount +=
      (product.quantity / product.stepper) *
      (product.price - (product.price * product.discount) / 100);
  }

  // Razorpay order creation
  var instance = new Razorpay({
    key_id: KEYID,
    key_secret: KEYSECRET,
  });

  var options = {
    amount: totalAmount * 100 + shippingTotal * 100,
    currency: "INR",
    receipt: "order_rcptid_" + req.body.orderId,
  };

  const paymentOrder = await instance.orders.create(options);
  if (!paymentOrder) return res.status(400).send("Error in creating order");
  //Razorpay end

  const order = new Order({
    orderId: req.body.orderId,
    user: user._id,
    address: req.body.address,
    products: products,
    shipStatus: req.body.shipStatus,
    paymentStatus: req.body.paymentStatus,
    orderTotal: totalAmount,
    date: req.body.date,
    paymentOrderId: paymentOrder.id,
    timeslot: req.body.timeslot,
    shippingTotal: shippingTotal,
  });

  try {
    let task = Fawn.Task();
    task = task.save("orders", order);
    if (req.body.paymentStatus == "COD") {
      for (let i in products) {
        task = task.update(
          "products",
          { _id: products[i]._id },
          {
            $inc: { numberInStock: -products[i].quantity },
          }
        );
      }
    }
    if (req.body.shopFrom == "cart") {
      task = task.update("user1", { _id: user._id }, { cart: [] });
    }
    task.run();

    if (req.body.paymentStatus == "COD") {
      smsClient.sendOrderConfirmationMessage(user, order);
    }

    res.send({
      order: {
        paymentOrderId: order.paymentOrderId,
        orderTotal: order.orderTotal,
        orderId: order.orderId,
        _id: order._id,
      },
      user: {
        mobile: user.mobile,
        name: user.name,
        email: user.email,
      },
    });
  } catch (ex) {
    res.status(500).send("Something failed.");
  }
});

router.post("/update", [auth], async (req, res) => {
  const { error } = validateUpdate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const order = await Order.findByIdAndUpdate(
    req.body._id,
    {
      paymentStatus: req.body.paymentStatus,
      timeslot: req.body.timeslot,
    },
    { new: true }
  );

  if (!order)
    return res
      .status(404)
      .send("The order with the given ID was not found." + req.params.id);

  if (req.body.paymentStatus == "COD" && req.body.sendsms) {
    const user = await User.findById(req.user._id);
    if (user) smsClient.sendOrderConfirmationMessage(user, order);
  }

  res.send(order.transform());
});

router.put("/:id", [auth, validateObjectId], async (req, res) => {
  const { error } = validateStatus(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const order = await Order.findByIdAndUpdate(
    req.params.id,
    {
      shipStatus: req.body.shipStatus,
      paymentStatus: req.body.paymentStatus,
    },
    { new: true }
  );

  if (!order)
    return res
      .status(404)
      .send("The order with the given ID was not found." + req.params.id);

  res.send(order.transform());
});

router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const order = await Order.findByIdAndRemove(req.params.id);

  if (!order)
    return res.status(404).send("The order with the given ID was not found.");

  res.send(order);
});

router.get("/:id", [auth, validateObjectId], async (req, res) => {
  const order = await Order.findById(req.params.id);

  if (!order)
    return res.status(404).send("The order with the given ID was not found.");

  var products = [];
  order.products.forEach((product) => {
    products.push(product.transformAll());
  });

  res.send(order.transformWithProducts(products));
});

router.post("/checkout", [auth], async (req, res) => {
  var instance = new Razorpay({
    key_id: KEYID,
    key_secret: KEYSECRET,
  });

  var options = {
    amount: 50000,
    currency: "INR",
    receipt: "order_rcptid_11",
  };
  const order = await instance.orders.create(options);
  res.send(order);
});

router.post("/update_stock_on_cod_cancel", [auth], async (req, res) => {
  const order = await Order.findById(req.body.id);
  if (!order)
    return res.status(404).send("The order with the given ID was not found.");

  if (order.paymentStatus !== "COD") {
    return res.status(404).send("Not allowed");
  }

  if (order.shipStatus !== "Cancelled") {
    return res.status(404).send("Not allowed");
  }

  try {
    // update products
    let task = Fawn.Task();
    let products = order.products;
    for (let i in products) {
      task = task.update(
        "products",
        { _id: products[i]._id },
        {
          $inc: { numberInStock: +products[i].quantity },
        }
      );
    }
    task.run();

    return res.send("Products stock Updated");
  } catch (ex) {
    console.log(ex);
    return res
      .status(404)
      .send("Error in stock update: " + ex.error.description);
  }
});

router.post("/refund", [auth], async (req, res) => {
  const order = await Order.findById(req.body.id);
  if (!order)
    return res.status(404).send("The order with the given ID was not found.");

  var instance = new Razorpay({
    key_id: KEYID,
    key_secret: KEYSECRET,
  });

  if (!order.paymentPayId) {
    return res.status(404).send("Payment for this order not found");
  }

  if (order.paymentStatus !== "Success") {
    return res.status(404).send("Payment for this order not found");
  }

  if (order.shipStatus !== "Ordered" && order.shipStatus !== "Cancelled") {
    return res.status(404).send("Shipping status is not Ordered or Cancelled");
  }

  var options = {
    amount: order.orderTotal * 100 + order.shippingTotal * 100,
  };

  // const payment = await instance.orders.fetchPayments(order.paymentOrderId);
  // console.log(payment);

  try {
    await instance.payments.refund(order.paymentPayId, options);

    // update products
    let task = Fawn.Task();
    let products = order.products;
    for (let i in products) {
      task = task.update(
        "products",
        { _id: products[i]._id },
        {
          $inc: { numberInStock: +products[i].quantity },
        }
      );
    }
    task.run();

    // Update order
    const orderUpdated = await Order.findByIdAndUpdate(
      order.id,
      {
        paymentPayId: "",
        paymentSignature: "",
        paymentStatus: "Refunded",
      },
      { new: true }
    );
    if (!orderUpdated) return res.status(500).send("Something failed.");

    return res.send("Refund Success");
  } catch (ex) {
    console.log(ex);
    return res.status(404).send("Error in refund: " + ex.error.description);
  }
});

router.post("/verifypurchase", [auth], async (req, res) => {
  const { error } = validateVerifyPurchase(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const order = await Order.findById(req.body._id);
  const user = await User.findById(req.user._id);

  if (!order)
    return res.status(404).send("The order with the given ID was not found.");

  // Verify signature
  const hash = crypto
    .createHmac("sha256", KEYSECRET)
    .update(order.paymentOrderId + "|" + req.body.paymentPayId)
    .digest("hex");

  if (hash != req.body.paymentSignature) return res.send("failed");
  console.log("signature verified" + req.body._id);

  // Update orders and products
  try {
    let task = Fawn.Task();
    let products = order.products;
    for (let i in products) {
      task = task.update(
        "products",
        { _id: products[i]._id },
        {
          $inc: { numberInStock: -products[i].quantity },
        }
      );
    }
    task.run();

    // Update in db
    const orderUpdated = await Order.findByIdAndUpdate(
      req.body._id,
      {
        paymentPayId: req.body.paymentPayId,
        paymentSignature: req.body.paymentSignature,
        paymentStatus: "Success",
      },
      { new: true }
    );
    if (!orderUpdated) return res.status(500).send("Something failed.");

    smsClient.sendOrderConfirmationMessage(user, order);

    res.send("success");
  } catch (ex) {
    res.status(500).send("Something failed.");
  }
});

router.get("/me/orders", auth, async (req, res) => {
  const user = await User.findById(req.user._id).select("-password");
  if (!user)
    return res.status(404).send("The user with the given ID was not found.");

  let orders = [];
  await Order.find(
    { user: req.user._id, paymentStatus: { $ne: "OOR" } },
    (err, result) => {
      for (let i = 0; i < result.length; i++) {
        let products = [];
        for (let j = 0; j < result[i].products.length; j++) {
          products.push(result[i].products[j].addKey(j));
        }
        let orderWithKey = result[i].addKey(i);
        orderWithKey.products = products;
        orders.push(orderWithKey);
      }
      res.send(orders);
    }
  ).sort({
    date: "desc",
  });
});

module.exports = router;
