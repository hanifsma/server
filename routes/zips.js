const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const { Zip, validate } = require("../models/zip");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  let query = {};
  if (req.query.q) {
    query.name = new RegExp(req.query.q, "i");
  }
  if (req.query.id) {
    query._id = req.query.id;
  }
  let offset = Number(req.query._start);
  let limit = Number(req.query._end) - offset;

  if (!offset) offset = 0;
  if (!limit) limit = 9999;

  await Zip.paginate(
    query,
    { offset: offset, limit: limit, sort: { _id: -1 } },
    (err, result) => {
      if (result) {
        let zips = [];
        for (let i = 0; i < result.docs.length; i++) {
          zips.push(result.docs[i].transform());
        }
        res
          .header("X-Total-Count", result.total)
          .header("Access-Control-Expose-Headers", "X-Total-Count")
          .send(zips);
      } else {
        res.status(400).send("No results");
      }
    }
  );
});

router.post("/", [auth], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const _zip = await Zip.findById(req.body.id);
  if (_zip) {
    return res.status(404).send("The zip already exist.");
  }

  let zip = new Zip({
    _id: req.body.id,
    state: req.body.state,
    city: req.body.city,
    loc: req.body.loc,
    available: req.body.available,
    outOfReach: req.body.outOfReach,
  });
  zip = await zip.save();

  res.send(zip.transform());
});

router.put("/:id", [auth], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const zip = await Zip.findByIdAndUpdate(
    req.params.id,
    {
      state: req.body.state,
      city: req.body.city,
      loc: req.body.loc,
      available: req.body.available,
      outOfReach: req.body.outOfReach,
    },
    { new: true }
  );

  if (!zip)
    return res
      .status(404)
      .send("The zip with the given ID was not found." + req.params.id);

  res.send(zip.transform());
});

router.delete("/:id", [auth], async (req, res) => {
  const zip = await Zip.findByIdAndRemove(req.params.id);

  if (!zip)
    return res.status(404).send("The zip with the given ID was not found.");

  res.send(zip);
});

router.get("/:id", [auth], async (req, res) => {
  const zip = await Zip.findById(req.params.id);

  if (!zip)
    return res.send({
      code: 201,
      message: "The zip with the given ID was not found.",
    });
  res.send(zip.transform());
});

module.exports = router;
