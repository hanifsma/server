const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const validateObjectId = require("../middleware/validateObjectId");
const { Product, validate } = require("../models/product");
const { Category } = require("../models/category");
const { Promotion } = require("../models/promotion");
const express = require("express");
const router = express.Router();
const { uploadFiles } = require("../utils/awss3upload");

router.get("/", async (req, res) => {
  let query = {};
  if (req.query.q) {
    query.name = new RegExp(req.query.q, "i");
  }
  const offset = Number(req.query._start);
  const limit = Number(req.query._end) - offset;

  await Product.paginate(
    query,
    { offset: offset, limit: limit, sort: { _id: -1 } },
    (err, result) => {
      let products = [];
      for (let i = 0; i < result.docs.length; i++) {
        products.push(result.docs[i].transformAll());
      }
      res
        .header("X-Total-Count", result.total)
        .header("Access-Control-Expose-Headers", "X-Total-Count")
        .send(products);
    }
  );

  // const products = await Product.find().sort("name");
  // res.send(products);
});

router.get("/filtered", async (req, res) => {
  let products = [];
  let combos = [];
  let promotions = [];

  let productsTemp = [];
  let combosTemp = [];
  let promotionsTemp = [];

  // numberInStock: { $gte: 1 },
  productsTemp = await Product.find({
    isCombo: false,
    isAvailable: true,
  }).sort("name");
  combosTemp = await Product.find({
    isCombo: true,
    isAvailable: true,
  }).sort("name");
  promotionsTemp = await Promotion.find({ available: true });

  for (let i = 0; i < productsTemp.length; i++) {
    products.push(productsTemp[i].transform());
  }
  for (let i = 0; i < combosTemp.length; i++) {
    combos.push(combosTemp[i].transform());
  }
  for (let i = 0; i < promotionsTemp.length; i++) {
    promotions.push(promotionsTemp[i].transform());
  }

  const productsObject = {
    id: "1",
    title: "Products",
    innerArray: products,
  };
  const combosObject = { id: "2", title: "Combo", innerArray: combos };
  const promotionsObject = { id: "3", title: "ads", innerArray: promotions };
  const result = [promotionsObject, productsObject, combosObject];
  res.send(result);
});

router.post("/", [auth, admin], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const category = await Category.findById(req.body.category.id);
  if (!category) return res.status(400).send("Invalid Category");

  let images = [];
  let imagesToUpload = [];
  req.body.images.forEach((element) => {
    if (element.src) {
      imagesToUpload.push(element);
    }
  });

  if (imagesToUpload.length > 0) {
    try {
      const filesUploaded = await uploadFiles(imagesToUpload);
      filesUploaded.forEach((element) => {
        images.push(element.Location);
      });
    } catch (error) {
      return res.status(404).send("Error in product image upload");
    }
  }

  const product = new Product({
    name: req.body.name,
    description: req.body.description,
    category: {
      _id: category._id,
      name: category.name,
    },
    price: req.body.price,
    unit: req.body.unit,
    discount: req.body.discount,
    numberInStock: req.body.numberInStock,
    isCombo: req.body.isCombo,
    description: req.body.description,
    isAvailable: req.body.isAvailable,
    stepper: req.body.stepper,
    images: images,
  });
  await product.save();

  res.send(product.transformAll());
});

router.put("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const category = await Category.findById(req.body.category.id);
  if (!category) return res.status(400).send("Invalid category.");

  let images = [];
  let imagesToUpload = [];
  req.body.images.forEach((element) => {
    if (element.url) {
      images.push(element.url);
    } else if (element.src) {
      imagesToUpload.push(element);
    }
  });

  if (imagesToUpload.length > 0) {
    try {
      const filesUploaded = await uploadFiles(imagesToUpload);
      filesUploaded.forEach((element) => {
        images.push(element.Location);
      });
    } catch (error) {
      return res.status(404).send("Error in product image upload");
    }
  }

  const product = await Product.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      description: req.body.description,
      category: {
        _id: category._id,
        name: category.name,
      },
      price: req.body.price,
      unit: req.body.unit,
      discount: req.body.discount,
      numberInStock: req.body.numberInStock,
      isAvailable: req.body.isAvailable,
      isCombo: req.body.isCombo,
      stepper: req.body.stepper,
      images: images,
    },
    { new: true }
  );

  if (!product)
    return res.status(404).send("The product with the given ID was not found.");

  console.log("4");

  res.send(product.transformAll());
});

router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const product = await Product.findByIdAndRemove(req.params.id);

  if (!product)
    return res.status(404).send("The product with the given ID was not found.");

  res.send(product);
});

router.get("/:id", [validateObjectId], async (req, res) => {
  const product = await Product.findById(req.params.id);

  if (!product)
    return res.status(404).send("The product with the given ID was not found.");

  res.send(product.transformAll());
});

module.exports = router;
