const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const validateObjectId = require("../middleware/validateObjectId");
const { Promotion, validate, validateEdit } = require("../models/promotion");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();
const { uploadFiles } = require("../utils/awss3upload");

router.get("/", async (req, res) => {
  // const promotions = await Promotion.find();
  // res.send(promotions);

  let query = {};
  if (req.query.q) {
    query.name = new RegExp(req.query.q, "i");
  }
  let offset = Number(req.query._start);
  let limit = Number(req.query._end) - offset;

  if (!offset) offset = 0;
  if (!limit) limit = 9999;

  await Promotion.paginate(
    query,
    { offset: offset, limit: limit, sort: { _id: -1 } },
    (err, result) => {
      if (result) {
        let promotions = [];
        for (let i = 0; i < result.docs.length; i++) {
          promotions.push(result.docs[i].transform());
        }
        res
          .header("X-Total-Count", result.total)
          .header("Access-Control-Expose-Headers", "X-Total-Count")
          .send(promotions);
      } else {
        res.status(400).send("No results");
      }
    }
  );
});

router.post("/", [auth, admin], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let image;
  let imagesToUpload = [];
  if (req.body.image.src) {
    imagesToUpload.push(req.body.image);
  }

  if (imagesToUpload.length > 0) {
    try {
      const filesUploaded = await uploadFiles(imagesToUpload);
      filesUploaded.forEach((element) => {
        image = element.Location;
      });
    } catch (error) {
      return res.status(404).send("Error in promotion image upload");
    }
  }

  let promotion = new Promotion({
    image: image,
    link: req.body.link,
    available: req.body.available,
  });
  agent = await promotion.save();

  res.send(promotion.transform());
});

router.put("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const { error } = validateEdit(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let image;
  let imagesToUpload = [];
  if (req.body.image.url) {
    image = req.body.image.url;
  } else if (req.body.image.src) {
    imagesToUpload.push(req.body.image);
  }

  if (imagesToUpload.length > 0) {
    try {
      const filesUploaded = await uploadFiles(imagesToUpload);
      filesUploaded.forEach((element) => {
        image = element.Location;
      });
    } catch (error) {
      return res.status(404).send("Error in promotion image upload");
    }
  }

  const promotion = await Promotion.findByIdAndUpdate(
    req.params.id,
    {
      available: req.body.available,
      link: req.body.link,
      image: image,
    },
    { new: true }
  );

  res.send(promotion.transform());
});

router.delete("/:id", async (req, res) => {
  const promotion = await Promotion.findByIdAndRemove(req.params.id);

  if (!promotion)
    return res
      .status(404)
      .send("The promotion with the given ID was not found.");

  res.send(promotion);
});

router.get("/:id", [validateObjectId, auth, admin], async (req, res) => {
  const promotion = await Promotion.findById(req.params.id);

  if (!promotion)
    return res
      .status(404)
      .send("The promotion with the given ID was not found.");

  res.send(promotion.transformWithImage());
});

module.exports = router;
