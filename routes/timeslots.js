const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const validateObjectId = require("../middleware/validateObjectId");
const { Timeslot, validate, validateEdit } = require("../models/timeslot");
const express = require("express");
const { result } = require("lodash");
const router = express.Router();

router.get("/", async (req, res) => {
  let query = {};
  if (req.query.q) {
    query.name = new RegExp(req.query.q, "i");
  }
  if (req.query.id) {
    query._id = req.query.id;
  }
  if (req.query.available) {
    query.available = true;
  }
  let offset = Number(req.query._start);
  let limit = Number(req.query._end) - offset;

  if (!offset) offset = 0;
  if (!limit) limit = 9999;

  await Timeslot.paginate(
    query,
    { offset: offset, limit: limit, sort: { _id: -1 } },
    (err, result) => {
      if (result) {
        let timeslots = [];
        for (let i = 0; i < result.docs.length; i++) {
          timeslots.push(result.docs[i].transform());
        }
        res
          .header("X-Total-Count", result.total)
          .header("Access-Control-Expose-Headers", "X-Total-Count")
          .send(timeslots);
      } else {
        res.status(400).send("No results");
      }
    }
  );
});

router.post("/", auth, async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let timeslot = new Timeslot({
    name: req.body.name,
    startTime: req.body.startTime,
    endTime: req.body.endTime,
    available: req.body.available,
  });
  try {
    timeslot = await timeslot.save();
    res.send(timeslot.transform());
  } catch (error) {
    res.status(404).send(error);
  }
});

router.put("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const { error } = validateEdit(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const timeslot = await Timeslot.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      startTime: req.body.startTime,
      endTime: req.body.endTime,
      available: req.body.available,
    },
    {
      new: true,
    }
  );

  if (!timeslot)
    return res
      .status(404)
      .send("The timeslot with the given ID was not found.");

  res.send(timeslot.transform());
});

router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const timeslot = await Timeslot.findByIdAndRemove(req.params.id);

  if (!timeslot)
    return res
      .status(404)
      .send("The timeslot with the given ID was not found.");

  res.send(timeslot);
});

router.get("/:id", validateObjectId, async (req, res) => {
  let timeslot = await Timeslot.findById(req.params.id);
  timeslot = timeslot.transform();

  if (!timeslot)
    return res
      .status(404)
      .send("The timeslot with the given ID was not found.");

  res.send(timeslot);
});

module.exports = router;
