const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const validateObjectId = require("../middleware/validateObjectId");
const { Category, validate, validateEdit } = require("../models/category");
const express = require("express");
const { result } = require("lodash");
const router = express.Router();

router.get("/", async (req, res) => {
  let query = {};
  if (req.query.q) {
    query.name = new RegExp(req.query.q, "i");
  }
  if (req.query.id) {
    query._id = req.query.id;
  }
  let offset = Number(req.query._start);
  let limit = Number(req.query._end) - offset;

  if (!offset) offset = 0;
  if (!limit) limit = 9999;

  await Category.paginate(
    query,
    { offset: offset, limit: limit, sort: { _id: -1 } },
    (err, result) => {
      if (result) {
        let categories = [];
        for (let i = 0; i < result.docs.length; i++) {
          categories.push(result.docs[i].transform());
        }
        res
          .header("X-Total-Count", result.total)
          .header("Access-Control-Expose-Headers", "X-Total-Count")
          .send(categories);
      } else {
        res.status(400).send("No results");
      }
    }
  );
});

router.post("/", auth, async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let category = new Category({ name: req.body.name });
  category = await category.save();

  res.send(category.transform());
});

router.put("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const { error } = validateEdit(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const category = await Category.findByIdAndUpdate(
    req.params.id,
    { name: req.body.name },
    {
      new: true,
    }
  );

  if (!category)
    return res
      .status(404)
      .send("The category with the given ID was not found.");

  res.send(category.transform());
});

router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const category = await Category.findByIdAndRemove(req.params.id);

  if (!category)
    return res
      .status(404)
      .send("The category with the given ID was not found.");

  res.send(category);
});

router.get("/:id", validateObjectId, async (req, res) => {
  let category = await Category.findById(req.params.id);
  category = category.transform();

  if (!category)
    return res
      .status(404)
      .send("The category with the given ID was not found.");

  res.send(category);
});

module.exports = router;
