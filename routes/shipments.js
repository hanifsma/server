const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const validateObjectId = require("../middleware/validateObjectId");
const { Shipment, validate, validateStatus } = require("../models/shipment");
const { Order } = require("../models/order");
const { Agent } = require("../models/agent");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  const shipments = await Shipment.find();
  res.send(shipments);
});

router.post("/", [auth, validateObjectId], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const order = await Order.findById(req.body.orderId);
  if (!order) return res.status(400).send("Invalid order.");

  const agent = await Agent.findById(req.body.agentId);
  if (!agent) return res.status(400).send("Invalid agent.");

  let shipment = new Shipment({
    name: req.body.name,
    order: order,
    agent: agent,
    pickupTime: req.body.pickupTime,
    deliverTime: req.body.deliverTime,
  });
  shipment = await shipment.save();

  res.send(shipment);
});

router.put("/:id", [auth, validateObjectId], async (req, res) => {
  const { error } = validateStatus(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const shipment = await Shipment.findByIdAndUpdate(
    req.params.id,
    {
      pickupTime: req.body.pickupTime,
      deliverTime: req.body.deliverTime,
    },
    { new: true }
  );

  if (!shipment)
    return res
      .status(404)
      .send("The shipment with the given ID was not found.");

  res.send(shipment);
});

router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const shipment = await Shipment.findByIdAndRemove(req.params.id);

  if (!shipment)
    return res
      .status(404)
      .send("The shipment with the given ID was not found.");

  res.send(shipment);
});

router.get("/:id", [validateObjectId], async (req, res) => {
  const shipment = await Shipment.findById(req.params.id);

  if (!shipment)
    return res
      .status(404)
      .send("The shipment with the given ID was not found.");

  res.send(shipment);
});

module.exports = router;
